package com.demo.service;

import org.springframework.stereotype.Service;

import com.demo.bean.User;

@Service
public interface UserService {
	public int saveUserData(User user);

	public User getUserData(int id);

	public int removeUserData(int id);

	public int updateUserData(int id, User user);
}
