package com.demo.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.bean.User;
import com.demo.repository.UserRepository;
import com.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public int saveUserData(User user) {
		User savedUser = null;
		try {
			savedUser = userRepository.save(user);
			return savedUser.getId();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return -1;
	}

	@Override
	public User getUserData(int id) {
		User user = null;
		try {
			user = userRepository.findUserById(id);
			return user;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public int removeUserData(int id) {
		try {
			userRepository.deleteById(id);
			return 1;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}

	@Override
	public int updateUserData(int id, User user) {
		User oldUserData;
		try {
			oldUserData = userRepository.findUserById(id);
			oldUserData.setFirstName(user.getFirstName());
			oldUserData.setLastName(user.getLastName());
			oldUserData.setEmail(user.getEmail());
			oldUserData.setCity(user.getCity());
			userRepository.save(oldUserData);
			return 1;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}

}
