package com.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.bean.User;
import com.demo.response.UserResponse;
import com.demo.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/userData", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> storeUserData(@RequestBody User bean) {
		ResponseEntity<?> entity = null;
		UserResponse response = new UserResponse();
		if (bean != null) {
			int id = userService.saveUserData(bean);
			response.setMessage("User Data Accepted with id : " + id);
			entity = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		return entity;
	}

	@CrossOrigin
	@RequestMapping(value = "/userData/id={id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getUserData(@PathVariable int id) {
		ResponseEntity<?> entity = null;
		UserResponse response = new UserResponse();
		User user = userService.getUserData(id);
		if (user != null) {
			response.setMessage("User Data Retrived Successfully");
			response.setUsers(user);
			entity = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		} else {
			response.setMessage("No record Found");
			entity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		return entity;
	}

	@RequestMapping(value = "/userData/id={id}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> removeUserData(@PathVariable int id) {
		ResponseEntity<?> entity = null;
		UserResponse response = new UserResponse();
		int i = userService.removeUserData(id);
		if (i == 1) {
			response.setMessage("User Data Deteled");
			entity = new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			response.setMessage("No record Found");
			entity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		return entity;
	}

	@RequestMapping(value = "/userData/id={id}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> updateUserData(@PathVariable int id, @RequestBody User user) {
		ResponseEntity<?> entity = null;
		UserResponse response = new UserResponse();
		int i = userService.updateUserData(id, user);
		if (i == 1) {
			response.setMessage("User Data Updated Successfully");
			entity = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		} else {
			response.setMessage("Unable to update User Data");
			entity = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}
}
