package com.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.demo.bean.User;
import com.demo.service.UserService;

@Component
public class TestRun implements CommandLineRunner {

	@Autowired
	UserService userService;

	@Override
	public void run(String... args) throws Exception {
		User user = new User();
		user.setFirstName("Sunny");
		user.setLastName("Singh");
		user.setEmail("sunnykr.singh@hotmail.com");
		user.setCity("Hyderabad");
		userService.saveUserData(user);
	}
}